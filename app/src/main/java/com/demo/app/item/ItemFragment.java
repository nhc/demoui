package com.demo.app.item;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListView;

import com.demo.app.R;
import com.demo.app.components.WaveView;

import java.util.ArrayList;

public class ItemFragment extends Fragment
{
    private ListView           listView;
    private ArrayList<RowItem> rowItems;
    private ItemAdapter        adapter;

    private SearchView         searchView;

    public ItemFragment()
    {
    }


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_item, container, false);

        // change search view design
        searchView = view.findViewById(R.id.search_view);
        searchView.setQueryHint("Search");
        searchView.setIconified(false);

        // Show list
        listView = view.findViewById(R.id.list_item);
        rowItems = new ArrayList<>();
        adapter = new ItemAdapter(getActivity(), rowItems);

        listView.setAdapter(adapter);

        createDemo();

        return view;
    }


    @Override
    public void onResume()
    {
        super.onResume();
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

    }

    @Override
    public void onDetach()
    {
        super.onDetach();
    }

    private void createDemo()
    {
        for (int i = 0; i < 10; i++)
        {
            RowItem item = new RowItem();

            item.setAction("CS:GO");
            item.setPrice("400");
            item.setTitle("G35G1 | Pola Game");
            item.setContent("Minimal Wear");
            item.setIcon(getResources().getDrawable(R.mipmap.img_item_sample));

            rowItems.add(item);
        }

        adapter.notifyDataSetChanged();
    }
}
