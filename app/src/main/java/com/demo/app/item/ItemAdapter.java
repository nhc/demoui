package com.demo.app.item;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.demo.app.R;

import java.util.ArrayList;

public class ItemAdapter extends BaseAdapter
{
    private Context            context;
    private ArrayList<RowItem> rowItems;
    private LayoutInflater     inflater;

    public ItemAdapter(Context context, ArrayList<RowItem> rowItems)
    {
        this.context = context;
        this.rowItems = rowItems;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount()
    {
        return rowItems.size();
    }

    @Override
    public RowItem getItem(int position)
    {
        return rowItems.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.row_item, null);
            Holder holder = new Holder();
            holder.icon = convertView.findViewById(R.id.icon);
            holder.title = convertView.findViewById(R.id.title);
            holder.content = convertView.findViewById(R.id.content);
            holder.price = convertView.findViewById(R.id.price);
            holder.action = convertView.findViewById(R.id.action);
            convertView.setTag(holder);
        }

        Holder  holder = (Holder) convertView.getTag();
        RowItem item   = getItem(position);

        holder.icon.setBackground(item.getIcon());

        holder.title.setText(item.getTitle());
        holder.content.setText(item.getContent());
        holder.price.setText(item.getPrice());
        holder.action.setText(item.getAction());

        return convertView;
    }

    class Holder
    {
        public TextView icon;
        public TextView title;
        public TextView content;
        public TextView price;
        public TextView action;
    }
}
