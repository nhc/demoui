package com.demo.app.earn;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.demo.app.R;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class EarnFragment extends Fragment
{
    LinearLayout lastEarn;

    public EarnFragment()
    {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_earn, container, false);
        lastEarn = view.findViewById(R.id.last_earn_list);

        createDemo();

        return view;
    }

    private void addEarnItem(EarnItem earnItem)
    {
        LayoutInflater inflater = getLayoutInflater();
        View           view     = inflater.inflate(R.layout.earn_row_item, null);
        TextView       title    = view.findViewById(R.id.title);
        TextView       time     = view.findViewById(R.id.time);
        TextView       point    = view.findViewById(R.id.point);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        title.setText(earnItem.getLabel());
        time.setText(df.format(new Date(earnItem.getTime())));
        point.setText(String.valueOf(earnItem.getPoint()));

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                                                                             ViewGroup.LayoutParams.WRAP_CONTENT);

        params.setMargins(0, 0, 0, 20);

        lastEarn.addView(view, params);
    }

    private void createDemo()
    {
        for (int i = 0; i < 5; i++)
        {
            EarnItem earnItem = new EarnItem();
            earnItem.setLabel("a" + i);
            earnItem.setPoint(400);
            earnItem.setTime(System.currentTimeMillis());

            addEarnItem(earnItem);
        }
    }
}
