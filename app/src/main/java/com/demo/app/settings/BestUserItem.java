package com.demo.app.settings;

import android.graphics.drawable.Drawable;

public class BestUserItem
{
    private String   username;
    private int      rank;
    private int      point;
    private Drawable avatar;

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public int getRank()
    {
        return rank;
    }

    public void setRank(int rank)
    {
        this.rank = rank;
    }

    public int getPoint()
    {
        return point;
    }

    public void setPoint(int point)
    {
        this.point = point;
    }

    public Drawable getAvatar()
    {
        return avatar;
    }

    public void setAvatar(Drawable avatar)
    {
        this.avatar = avatar;
    }
}
