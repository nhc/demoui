package com.demo.app.settings;

import android.graphics.drawable.Drawable;

public class RecentUserItem
{
    private String username;
    private Drawable avatar;

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public Drawable getAvatar()
    {
        return avatar;
    }

    public void setAvatar(Drawable avatar)
    {
        this.avatar = avatar;
    }
}
