package com.demo.app.settings;


import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.demo.app.R;

public class SettingFragment extends Fragment
{
    private LinearLayout recentList;
    private LinearLayout bestUser;

    public SettingFragment()
    {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_setting, container, false);

        recentList = view.findViewById(R.id.recent_list);
        bestUser = view.findViewById(R.id.best_user_list);

        createRecentDemo();
        createBestDemo();

        return view;
    }

    private void addRecentUser(RecentUserItem userItem)
    {
        LayoutInflater inflater = getLayoutInflater();
        View           view     = inflater.inflate(R.layout.user, null);

        TextView  username = view.findViewById(R.id.username);
        ImageView avatar   = view.findViewById(R.id.avatar);

        username.setText(userItem.getUsername());
        avatar.setImageDrawable(userItem.getAvatar());

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                                                                             ViewGroup.LayoutParams.WRAP_CONTENT);

        params.setMargins(0, 0, 20, 0);

        recentList.addView(view, params);
    }

    private void createRecentDemo()
    {
        for (int i = 0; i < 2; i++)
        {
            RecentUserItem item = new RecentUserItem();

            item.setUsername("username");
            item.setAvatar(getResources().getDrawable(R.mipmap.img_item_sample));

            addRecentUser(item);
        }
    }


    private void addBestUser(BestUserItem userItem)
    {
        LayoutInflater inflater = getLayoutInflater();
        View           view     = inflater.inflate(R.layout.best_user_row_item, null);

        TextView  username = view.findViewById(R.id.username);
        TextView  rank     = view.findViewById(R.id.rank);
        TextView  point    = view.findViewById(R.id.point);
        ImageView avatar   = view.findViewById(R.id.imageView1);


        username.setText(userItem.getUsername());
        rank.setText("Rank: " + String.valueOf(userItem.getRank()));
        avatar.setImageDrawable(userItem.getAvatar());
        point.setText(String.valueOf(userItem.getPoint()));

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                                                                             ViewGroup.LayoutParams.WRAP_CONTENT);

        params.setMargins(0, 0, 0, 20);

        bestUser.addView(view, params);
    }

    private void createBestDemo()
    {
        for (int i = 0; i < 3; i++)
        {
            BestUserItem item = new BestUserItem();

            item.setUsername("username");
            item.setAvatar(getResources().getDrawable(R.mipmap.img_item_sample));
            item.setPoint(400);
            item.setRank(i);

            addBestUser(item);
        }
    }
}
