package com.demo.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.demo.app.earn.EarnFragment;
import com.demo.app.item.ItemFragment;
import com.demo.app.messagebox.BoxActivity;
import com.demo.app.profile.ProfileFragment;
import com.demo.app.settings.SettingFragment;

public class MainActivity extends AppCompatActivity
{


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);

        try
        {
            getSupportActionBar().hide();
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        BottomNavigationView bottomNavigationView = findViewById(R.id.navigation);
        bottomNavigationView.setItemIconTintList(null);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener()
        {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem)
            {
                FragmentManager fragmentManager = getSupportFragmentManager();

                switch (menuItem.getItemId())
                {
                    case R.id.navigation_skin:
                        //Hien thi giao dien item
                        ItemFragment itemFragment = new ItemFragment();
                        fragmentManager.beginTransaction().add(R.id.container, itemFragment).commit();
                        break;
                    case R.id.navigation_earn:
                        // Hien thi giao dien earn
                        EarnFragment earnFragment = new EarnFragment();
                        fragmentManager.beginTransaction().add(R.id.container, earnFragment).commit();
                        break;
                    case R.id.navigation_profile:
                        // Hien thi giao dien profile
                        ProfileFragment profileFragment = new ProfileFragment();
                        fragmentManager.beginTransaction().add(R.id.container, profileFragment).commit();
                        break;
                    case R.id.navigation_settings:
                        // Hien thi giao dien settings
                        SettingFragment settingFragment = new SettingFragment();
                        fragmentManager.beginTransaction().add(R.id.container, settingFragment).commit();
                        break;
                }
                return true;
            }
        });

        FragmentManager fragmentManager = getSupportFragmentManager();

        ItemFragment itemFragment = new ItemFragment();
        fragmentManager.beginTransaction().add(R.id.container, itemFragment).commit();

        // Hien thi message box
//        Intent messageBox = new Intent(this, BoxActivity.class);
//        startActivity(messageBox);
    }
}
